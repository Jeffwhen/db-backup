Auto-backup shell script for mongodb
====================================

*THE CONFIG VARIABLES MUST BE SET*, the consequences of not initialize those variables have not been tested. Be careful.
Add the following line into crotab to make it run periodically.

```shell
0 0 * * * /db-backup/db-backup.sh
```

Configure `mail` application by following steps:

```shell
wget -c http://dl.fedoraproject.org/pub/epel/6/x86_64/epel-release-6-8.noarch.rpm
rpm -ivh epel-release-6-8.noarch.rpm
yum install ssmtp
alternatives --config mta # choose ssmtp
```

in `/etc/ssmtp/ssmtp.conf` add
```
mailhub=mail.yourdomain.com:587
Hostname=localhost
AuthUser=username@yourdomain.com
AuthPass=YourPassWord
UseTLS=YES
TLS_CA_File=/etc/pki/tls/certs/ca-bundle.crt
```

in `/etc/ssmtp/revaliases` add
```
root:username@gmail.com:smtp.gmail.com:587
```
