#!/bin/sh
DIR=/backup/path/
DB_NAME=dbname
HOST=localhost
PORT=27017
COLS="col1 col2"
KEPT_DAYS=15
MEXPORT=mongoexport
DB_BACKUP=$MEXPORT
LOG_FILE=/path/to/tmp/log/file
ERR_FILE=/path/to/tmp/error/file
MAIL_SUB="subject of log mail"
MAIL_ADDRS="subscriber1@domain.com subscriber2@domain.com"
MONGO=mongo
source "$(dirname $0)/config"

trap "do_exit" EXIT TERM INT
set -eo pipefail
balancer_set=false

# initial log file
today=$(date +%Y/%m/%d-%H:%M)
echo "====== LOG FILE $today =====" > $LOG_FILE
echo "====== ERR FILE $today =====" > $ERR_FILE
echo "" >> $ERR_FILE
exec 6>&1
exec 7>&2
exec 2>>"$ERR_FILE"
exec >>"$LOG_FILE"

# exit handling
# process log files and send to subscribers
do_exit () {
    if $balancer_set; then
        echo "[$(date +%Y/%m/%d-%H:%M:%S)] now turn shard balancer back on"
        $MONGO $HOST:$PORT --quiet --eval 'sh.setBalancerState(true)' > /dev/null
    fi
    cat $LOG_FILE >> $ERR_FILE
    cp $ERR_FILE "$DIR$(date +%Y%m%d).log"
    exec 1>&6 2>&7 6>&- 7>&-
    cat "$DIR$(date +%Y%m%d).log"
    for addr in $MAIL_ADDRS; do
        echo ""
        mail -s "$MAIL_SUB" $addr < $ERR_FILE
    done
    echo "[$(date +%Y/%m/%d-%H:%M:%S)] db-backup.sh exit signal"
}

# return last update date
# @param collection name
last_update () {
    oldest=$($MONGO $HOST:$PORT/$DB_NAME --quiet --eval \
        'db.'$1'.find({}, {createdAt: 1}).sort({createdAt: 1}).limit(1)') || true
    oldest=$(echo $oldest | grep -P '\d{4}-\d{2}-\d{2}' -o) || true
    echo $oldest
}

[[ ${DIR: -1} != \/ ]] && DIR="$DIR/"
if [ ! -e $DIR ]; then
    mkdir -p $DIR
fi

let days=0 || true
let start_date=$(date +%s) || true
for col in $COLS; do
    update=$(last_update $col)
    update=$(date +%s -d "$update")
    let d=($(date +%s)-$update)/86400 || true
    [ $update -le $start_date ] && let start_date=$update || true
    [ $d -ge $days ] && let days=d || true
done

let days=$days-$KEPT_DAYS || true
[ $days -lt 0 ] && let days=0 || true
echo "[$(date +%Y/%m/%d-%H:%M:%S)] $days days to backup."
echo "[$(date +%Y/%m/%d-%H:%M:%S)] start from $(date -d @$start_date +%Y/%m/%d-%H:%M:%S)"
balancer_state=$($MONGO $HOST:$PORT --quiet --eval 'sh.getBalancerState()' || true)

if [[ $balancer_state = "true" ]]; then
    echo "[$(date +%Y/%m/%d-%H:%M:%S)] turn off shard balancer"
    $MONGO $HOST:$PORT --quiet --eval 'sh.stopBalancer()' > /dev/null
    balancer_set=true
fi

OPTS="--db $DB_NAME --port $PORT --host $HOST"

let i=0 || true
pushd $DIR > /dev/null
while [ $i -lt $days ]; do
    let day=($start_date + i * 86400) || true
    let i=i+1 || true
    start=$(date -d @$day "+%Y-%m-%d %H:%M:%S")
    let day=($day + 86400) || true
    end=$(date "+%Y-%m-%d %H:%M:%S" -d @$day)

    SDIR="$(date -d "$start" +%Y%m%d)"
    if tar ztf $SDIR.tar.gz &> /dev/null; then
        echo "[$(date +%Y/%m/%d-%H:%M:%S)] day $start has already been fully backed up"
        continue
    fi
    rm -rf $SDIR
    for col in $COLS; do
        FILE="$SDIR/$col.backup"
        echo "[$(date +%Y/%m/%d-%H:%M:%S)] backing up: $col $start"
        $DB_BACKUP $OPTS --quiet --collection $col --query \
            "{createdAt: {\$lt: \"$end\", \$gte: \"$start\"}}" --out ./$FILE
    done
    echo "[$(date +%Y/%m/%d-%H:%M:%S)] packing up..."
    tar -czf $SDIR.tar.gz $SDIR
    rm -rf $SDIR
done
popd > /dev/null

let day=$start_date || true
start=$(date -d @$day "+%Y-%m-%d %H:%M:%S")
let day=($day + 86400 * $days) || true
end=$(date -d @$day "+%Y-%m-%d %H:%M:%S")

for col in $COLS; do
    echo "[$(date +%Y/%m/%d-%H:%M:%S)] removing: $col \$lt $end"
    $MONGO $HOST:$PORT/$DB_NAME --quiet --eval \
        "db.$col.remove({createdAt: {'\$lt': \"$end\"}})"
    num=$($MONGO $HOST:$PORT/$DB_NAME --quiet  --eval "db.$col.count()")
    echo "[$(date +%Y/%m/%d-%H:%M:%S)] $num documents remained in col $col."
done
echo "[$(date +%Y/%m/%d-%H:%M:%S)] exiting..."

